import React from 'react';
import logo from './logo.svg';
import './App.css';
import SimpleRawButton from './components/simpleRawButton';
import SimpleStyledButton from './components/simpleStyledButton';
import Button from '@material-ui/core/Button';
import TodoCard from './components/todoCard';

// This is our toplevel compontent --> Entrypoint of our App

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          A Raw HTML button
        </p>
        <SimpleRawButton myLabel="Click again"/>
        <p>
          A styled MUI Button as a component
        </p>
        <SimpleStyledButton myLabel="Do not click me"/>
        <p>
          A plain MUI Button as simple import
        </p>
        <Button  variant="contained" color={"primary"} onClick={()=>{alert("Hello World!")}}>MUI Button</Button>
        <TodoCard/>
      </header>
    </div>
  );
};

export default App;
