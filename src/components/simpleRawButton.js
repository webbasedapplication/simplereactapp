import React, {useState} from 'react';

const MyButton = (props) => {
    const [counter, setCounter] = useState(0);

    const handleOnClick = () =>  {
        console.log("Hello from MyButton");
        setCounter(counter +1);
    };

    return(
        <button onClick={handleOnClick}>
            {props.myLabel} is now {counter}
        </button>
    );
};

export default MyButton;
