// All components need to import React from 'react'
import React, {useState} from 'react';
// Material UI components
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';

// We define CSS styles here as a Javascript Object and pass it to MaterialUI Theme
const useStyles = makeStyles(theme => ({
    greenLabel: {
        color: "green"
    }
}));

const MyButton = (props) => {
    // Get access to the CSS styles and store it in a variable we could use
    const classes = useStyles();
    // React State Hook (https://reactjs.org/docs/hooks-state.html)
    const [counter, setCounter] = useState(0);

    // Define a Click-Handler function
    const handleOnClick = () =>  {
        console.log("Hello from MyButton");
        setCounter(counter +1);
    };

    // return the component
    return(
        <Button variant="contained" color={"primary"} onClick={handleOnClick}>
            {props.myLabel} is now <p className={classes.greenLabel}>{counter}</p>
        </Button>
    );
};
// Important to be importable!!!
export default MyButton;
