import React, {useState, useEffect} from 'react';
import axios from 'axios';

const TodoCard = () => {
    const [todo,setTodo] = useState({
        id: 0,
        title: "",
        description: ""
    });

    useEffect(() => {
        axios.get("http://localhost:8080/api/todo").then((result) =>{
            console.log(result.data);
            setTodo(result.data);
        }).catch((error) => {
            console.log(error);
        })
    },[]);

    return(
        <div>
            <h1>{todo.title}</h1>
            <p>{todo.description}</p>
        </div>
    );
};

export default TodoCard;
